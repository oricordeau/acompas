import { ref, watch } from 'vue'
import { Platform } from 'quasar'
import { KeepAwake } from '@capacitor-community/keep-awake'

export const useKeepAwake = () => {
  // if (Platform.is.capacitor && await isSupported()) {
  //   await KeepAwake.allowSleep()
  // }

  const screenLock = ref<WakeLockSentinel | null>(null)

  const isSupported = async () => {
    const screenWakeLockSupported = 'wakeLock' in navigator
    const keepAwakeSupported = (await KeepAwake.isSupported()).isSupported

    return screenWakeLockSupported || keepAwakeSupported
  }

  const isKeptAwake = async () => {
    const result = await KeepAwake.isKeptAwake()
    return result.isKeptAwake
  }

  const keepAwake = async () => {
    if (Platform.is.capacitor) {
      await KeepAwake.keepAwake()
    }

    else if ('wakeLock' in navigator) {
      try {
        screenLock.value = await navigator.wakeLock.request('screen')
      } catch (error) {
        console.error('Unable to acquire screen wake lock:', error)
      }
    }
  }

  const allowSleep = async () => {
    if (Platform.is.capacitor) {
      await KeepAwake.allowSleep()
    }

    else if ('wakeLock' in navigator) {
      try {
        if (screenLock.value) {
          screenLock.value.release()
        }
      } catch (error) {
        console.error('Unable to release screen wake lock:', error)
      }
    }
  }

  return {
    isSupported,
    isKeptAwake,
    keepAwake,
    allowSleep
  }
}
